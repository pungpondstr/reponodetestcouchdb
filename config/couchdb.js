const NodeCouchDb = require('node-couchdb');
require('dotenv').config();

const couch = new NodeCouchDb({
    host: '192.168.1.33',
    protocol: 'http',
    port: 5984,
    auth: {
        user: process.env.USER_LOGIN,
        password: process.env.PASSWORD
    }
});

module.exports = couch;