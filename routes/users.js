const express = require('express');
const route = express.Router();
const { check } = require('express-validator');
const Authentication = require('../middleware/Authentication');
const UserController = require('../controller/UserController');

route.post('/login', [
    check('user_id').not().notEmpty().withMessage('กรุณากรอก รหัสประจำตัวผู้ป่วย !'),
    check('password').not().notEmpty().withMessage('กรุณากรอก รหัสผ่าน !'),
], UserController.login);

route.post('/register', [
    check('user_id').not().notEmpty().withMessage('กรุณากรอก รหัสประจำตัวผู้ป่วย !'),
    check('password').not().notEmpty().withMessage('กรุณากรอก รหัสผ่าน !'),
    check('confirm_password').not().notEmpty().withMessage('กรุณากรอก ยืนยันรหัสผ่าน !'),
    check('sex').not().notEmpty().withMessage('กรุณาเลือกเพศ !'),
    check('birth_day').not().notEmpty().withMessage('กรุณาเลือก วัน/เดือน/ปี เกิด !'),
    check('height').not().notEmpty().withMessage('กรุณากรอก ส่วนสูง !'),
    check('weight').not().notEmpty().withMessage('กรุณากรอก น้ำหนัก !'),
    check('congenital_disease').not().notEmpty().withMessage('กรุณาเลือกโรคประจำตัว !'),
    check('glomerular_filtration_rate_value').not().notEmpty().withMessage('กรุณากรอกค่าอัตราการกรองไต !'),
    check('blood_sugar_level').not().notEmpty().withMessage('กรุณากรอกค่าระดับน้ำตาลสะสมในเลือด !'),
    check('blood_sugar_level_before_eating').not().notEmpty().withMessage('กรุณากรอกค่าระดับน้ำตาลในเลือดก่อนรับประทานอาหาร !')
], UserController.register);

route.get('/profile', [Authentication.isLogin], UserController.profile);

module.exports = route;