const express = require('express');
const route = express.Router();
const { check } = require('express-validator');
const Authentication = require('../middleware/Authentication');
const WeightController = require('../controller/WeightController');

route.post('/add', [Authentication.isLogin], WeightController.add_weight);

module.exports = route;