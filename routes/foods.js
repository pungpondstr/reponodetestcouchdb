const express = require('express');
const route = express.Router();
const { check } = require('express-validator');
const Authentication = require('../middleware/Authentication');
const FoodController = require('../controller/FoodController');

route.post('/add', FoodController.add_food);

route.put('/update', FoodController.update_food);

//route.get('/profile', [Authentication.isLogin], UserController.profile);

module.exports = route;