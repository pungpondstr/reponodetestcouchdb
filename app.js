const express = require('express');
const app = express();

const body = require('body-parser');
const cors = require('cors');
const socketio = require('socket.io');
const passport = require('passport');

require('dotenv').config();

app.use(body());
app.use(cors());
app.use(passport.initialize());


const index = require('./routes/index');
const users = require('./routes/users');
const foods = require('./routes/foods');
const weights = require('./routes/weights');

app.use('/', index);
app.use('/users', users);
app.use('foods', foods);
app.use('/weights', weights);

const server = app.listen(5000, () => console.log(`server start port: 5000`));

const io = socketio(server, {
    cors: {
        origin: '*',
    }
});

io.on('connection', (socket) => {
    console.log('user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});