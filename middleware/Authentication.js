const passport = require('passport');
const couchdb = require('../config/couchdb');

const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {}

require('dotenv').config();

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.SECRET_KEY;

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    couchdb.get('users', jwt_payload.id).then(({data, headers, status}) => {
        if (data) {
            return done(null, data);
        } else {
            return done(null, false);
        }
    }, (err) => {
        if (err) {
            return done(err, false);
        }
    })
}));

module.exports.isLogin = passport.authenticate('jwt', { session: false });
