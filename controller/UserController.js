const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bcrypt = require('bcryptjs');
const couchdb = require('../config/couchdb');
require('dotenv').config();

const login = async (req, res) => {
    const { user_id, password } = req.body;

    const errors = validationResult(req);

    const dbName = "users";

    const mangoQuery = {
        selector: {
            "user_id": req.body.user_id
        }
    };

    const parameters = {};

    if (!errors.isEmpty()) {
        return res.json(errors);
    } else {
        await couchdb.mango(dbName, mangoQuery, parameters).then(({ data, headers, status }) => {
            if (data.docs.length !== 0) {
                bcrypt.compare(password, data.docs[0].password, ((err, result) => {
                    if (user_id === data.docs[0].user_id && result === true) {
                        const token = jwt.sign({
                            id: data.docs[0]._id,
                        },
                            process.env.SECRET_KEY, {
                            expiresIn: '365 days'
                            //expiresIn: '60s'
                        });
    
                        const expires_in = jwt.decode(token);
    
                        res.json({
                            access_token: token, //token
                            expires_in: expires_in.exp, //วันหมดอายุ
                            token_type: 'Bearer'
                        });
                    } else {
                        res.json('รหัสผ่านผิดพลาด !')
                    }
                }));
            } else {
                res.json('ไม่พบผู้ใช้งานในระบบ !');
            }
        })
    }
}

const register = async (req, res) => {
    const { user_id, password, confirm_password, sex, birth_day, height, weight, congenital_disease, glomerular_filtration_rate_value, blood_sugar_level, blood_sugar_level_before_eating } = req.body;

    const errors = validationResult(req);

    const salt = await bcrypt.genSalt(10);
    
    const dbName = "users";

    const mangoQuery = {
        selector: {
            "user_id": req.body.user_id
        }
    };

    const parameters = {};

    if (password !== confirm_password) {
        res.json('รหัสผ่าน กับ ยืนยันรหัสผ่าน ไม่ตรงกัน !');
    } else if (!errors.isEmpty()) {
        return res.json(errors);
    } else {
        await couchdb.mango(dbName, mangoQuery, parameters).then(({ data, headers, status }) => {
            if (data.docs.length !== 0) {
                res.json('รหัสประจำตัวผู้ป่วยซ้ำกับฐานข้อมูล !');
            } else {
                bcrypt.hash(req.body.password, salt).then((hash) => {
                    couchdb.insert(dbName, {
                        user_id: user_id,
                        password: hash,
                        sex: sex,
                        birth_day: birth_day,
                        height: height,
                        weight: weight,
                        congenital_disease: congenital_disease,
                        glomerular_filtration_rate_value: glomerular_filtration_rate_value,
                        blood_sugar_level: blood_sugar_level,
                        blood_sugar_level_before_eating: blood_sugar_level_before_eating,
                        date_create: new Date().toLocaleString('TH', { timeZone: 'Asia/Bangkok' }),
                        foods: [],
                        pressures: [],
                        weights: [],
                        exercises: [],
                        medications: [],
                        symptoms: []
                    }).then(response => {
                        res.json('ลงทะเบียนสำเร็จ');
                    })
                })
            }
        });
    }
}

const profile = async (req, res) => {
    const data = req.user;
    res.json(data)
}

module.exports = {
    login,
    register,
    profile,
}