const couchdb = require('../config/couchdb');
require('dotenv').config();

const add_weight = async (req, res) => {
    // const data = {
    //     _id: req.user._id,
    //     _rev: req.user._rev,
    //     user_id: req.user.user_id,
    //     password: req.user.password,
    //     sex: req.user.sex,
    //     birth_day: req.user.birth_day,
    //     height: req.user.height,
    //     weight: req.user.weight,
    //     congenital_disease: req.user.congenital_disease,
    //     glomerular_filtration_rate_value: req.user.glomerular_filtration_rate_value,
    //     blood_sugar_level: req.user.blood_sugar_level,
    //     blood_sugar_level_before_eating: req.user.blood_sugar_level_before_eating,
    //     date_create: req.user.date_create,
    //     foods: req.user.food,
    //     pressures: req.user.pressures,
    //     weights: [],
    //     exercises: req.user.exercises,
    //     medications: req.user.medications,
    //     symptoms: req.user.symptoms
    // }

    const data = req.user;
    
    data.weights = req.user.weights;

    await couchdb.uniqid().then(ids => {
        data.weights.push({
            _id: ids[0],
            weight_value: req.body.weight_value,
            date_create: new Date().toLocaleString('TH', { timeZone: 'Asia/Bangkok' }),
        })
    });

    await couchdb.update('users', data).then(({ data, headers, status }) => {
        res.json('เพิ่มข้อมูลสำเร็จ');
    });
}

module.exports = {
    add_weight,
}